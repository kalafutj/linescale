#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	painted = true;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	painted = true;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	QPoint bod;

	if (painted&&event->button() == Qt::LeftButton) {
		if (V.size() > 0) 
			dda(V[V.size() - 1].x(), V[V.size() - 1].y(), event->pos().x(), event->pos().y());
		V.push_back(event->pos());
		update();
//		painting = true;
	}
	if (V.size()&&event->button() == Qt::RightButton) {
		dda(V[V.size() - 1].x(), V[V.size() - 1].y(), V[0].x(), V[0].y());
		rob();
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
/*	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}*/
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}


void PaintWidget::rob() {
	bool x=false;
	int n = V.size() - 1;
	std::vector<priamka> tah;
	priamka* P = new priamka[n+1];
	update();

	for (int i = 0;i < n;i++)
		if (V[i].y() < V[i + 1].y()) P[i].nastav(V[i], V[i + 1]);
		else P[i].nastav(V[i + 1], V[i]);

		if (V[0].y() < V[n].y()) P[n].nastav(V[0], V[n]);
		else P[n].nastav(V[n], V[0]);

	for (int y = 0;y <= 600;y++) {
		for (int i = 0;i <= n;i++) {
			if (!(P[i] != y)) {
				tah.push_back(P[i]);
				P[i].iter();
			}
		}
		if (!tah.empty())
			vypln(y, tah);
		tah.clear();
	}
	V.clear();
	delete[] P;
	update();
}

void PaintWidget::vypln(int y, std::vector<priamka> P) {

	std::sort(P.begin(), P.end());

	for (int i = 0;i < P.size();i+=2) {
		for (int x = P[i].X();x < P[i + 1].X();x++) {
			image.setPixelColor(x, y, myPenColor);
		}
		P[i].iter();
		P[i + 1].iter();
	}
}

void PaintWidget::dda(int zx, int zy, int kx, int ky) {

	if (abs(ky - zy) > abs(kx - zx)) {
		double k = (kx - zx) / (double)(ky - zy);
		double q = kx - k*ky;
		if (zy > ky) for (int y = zy; y > ky;y--) image.setPixelColor(k*y + q, y, myPenColor);
		else for (int y = zy;y < ky;y++) image.setPixelColor(k*y + q, y, myPenColor);
	}
	else {
		double k = (ky - zy) / (double)(kx - zx);
		double q = ky - k*kx;
		if (zx > kx)for (int x = zx;x > kx;x--) image.setPixelColor(x, k*x + q, myPenColor);
		else for (int x = zx;x < kx;x++) image.setPixelColor(x, k*x + q, myPenColor);
	}
}

void PaintWidget::posun(QPoint A, QPoint B) {
	QPoint vektor (B.x() - A.x(), B.y() - A.y());

	for (int i = 0; i < V.size(); i++) V[i] += vektor;

	clearImage();

	rob();
}

void PaintWidget::otocenie(double uhol) {
	double SIN = sin(uhol);
	double COS = cos(uhol);
	// treba dotrobit stred
	for (int i = 0; i < V.size(); i++) {
		V[i].setX((int)(COS*V[i].x() + SIN*V[i].y()));
		V[i].setY(-SIN*V[i].x() + COS*V[i].y());
	}
	clearImage();
	rob();
}

void PaintWidget::skalovanie(double k) {
	//treba stred
	for (int i = 0; i < V.size(); i++) {
		k*V[i];
	}
	clearImage();
	rob();
}

void PaintWidget::skosenieX(double b) {
	//treba stred
	for (int i = 0; i < V.size(); i++) {
		V[i].setX(V[i].x() - b*V[i].y);
	}
	clearImage();
	rob();
}

void PaintWidget::skosenieY(double c) {
	//treba stred
	for (int i=0;i<V.size();i++)
		V[i].setY(c*V[i].x() + V[i].y);
	
	clearImage();
	rob();
}
